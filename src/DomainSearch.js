import React from "react";

// Styles
import "./App.css";

// Custom Components
import { Autocomplete, SelectDomain } from "./components";

// Material Components
import {
  CircularProgress,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Paper,
} from "@material-ui/core";

// Hooks
import { useDomainSearch } from "./useDomainSearch";

function DomainSearch() {
  /** Custom hook with all business logic */
  const {
    domainsMemo,
    domains,
    loading,
    selectedDomain,
    setSelectedDomain,
    mainDomains,
    selectedMainDomain,
    handleChangeMainDomain,
  } = useDomainSearch();

  return (
    <div className="App">
      <div className="container">
        {loading ? (
          <CircularProgress className="loader" />
        ) : (
          <>
            <div className="controls">
              <Autocomplete
                initDomains={domains}
                domainsMemo={domainsMemo}
                selectDomain={setSelectedDomain}
              />
              <SelectDomain
                mainDomains={mainDomains}
                selectedMainDomain={selectedMainDomain}
                changeMainDomain={handleChangeMainDomain}
              />
            </div>
            {selectedDomain && (
              <TableContainer component={Paper} className="table-container">
                <Table>
                  <TableBody>
                    {Object.entries(selectedDomain)
                      .filter(
                        ([key, value]) =>
                          !!value &&
                          !(value instanceof Array && Object.keys(value[0]))
                      )
                      .map(([key, value]) => {
                        return (
                          <TableRow key={key}>
                            <TableCell component="th" scope="row">
                              {key}
                            </TableCell>
                            <TableCell align="right">{value}</TableCell>
                          </TableRow>
                        );
                      })}
                  </TableBody>
                </Table>
              </TableContainer>
            )}
          </>
        )}
      </div>
    </div>
  );
}

export default DomainSearch;
