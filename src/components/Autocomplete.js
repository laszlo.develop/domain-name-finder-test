import React from "react";
import { array, func } from "prop-types";

import { TextField } from "@material-ui/core";
import { Autocomplete as MaterialAutocomplete } from "@material-ui/lab";

const Autocomplete = ({ domainsMemo, initDomains, selectDomain }) => {
  const handleChange = (e, value) => {
    selectDomain(value);
  };

  return (
    <MaterialAutocomplete
      id="combo-box-demo"
      options={domainsMemo.length ? domainsMemo : initDomains}
      getOptionLabel={(option) => option.domain}
      style={{ width: 300 }}
      renderInput={(params) => (
        <TextField {...params} label="Search domains..." variant="outlined" />
      )}
      noOptionsText={"Sorry, no results :("}
      onChange={handleChange}
    />
  );
};

Autocomplete.propTypes = {
  domainsMemo: array.isRequired,
  initDomains: array.isRequired,
  selectDomain: func.isRequired,
};

export default Autocomplete;
