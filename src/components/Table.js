import React from "react";
import { array } from "prop-types";

import {
  Paper,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from "@material-ui/core";

const Table = ({ selectedDomainValues }) => {
  return (
    <TableContainer component={Paper} className="table-container">
      <Table>
        <TableBody>
          {selectedDomainValues.map(([key, value]) => {
            return (
              <TableRow key={key}>
                <TableCell component="th" scope="row">
                  {key}
                </TableCell>
                <TableCell align="right">{value}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

Table.propTypes = {
  selectedDomainValues: array,
};

export default React.memo(Table);
