import React from "react";
import { array, func, string } from "prop-types";

import { FormControl, InputLabel, MenuItem, Select } from "@material-ui/core";

const SelectDomain = ({
  mainDomains,
  selectedMainDomain,
  changeMainDomain,
}) => {
  return (
    <FormControl variant="outlined" className="form-control">
      <InputLabel id="demo-simple-select-outlined-label">Domain</InputLabel>
      <Select
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined"
        value={selectedMainDomain}
        onChange={changeMainDomain}
      >
        <MenuItem value={""}>All domains</MenuItem>
        {mainDomains.map((item) => (
          <MenuItem key={item} value={item}>
            {item}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

SelectDomain.propTypes = {
  mainDomains: array.isRequired,
  selectedMainDomain: string.isRequired,
  changeMainDomain: func,
};

export default SelectDomain;
