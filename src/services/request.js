const DEFAULT_HEADERS = {
  Accept: "application/json",
  "Content-Type": "application/json;charset=utf-8",
  "Access-Control-Allow-Origin": "*",
};

const BASE_URL = 'https://api.domainsdb.info/v1';

class Request {
  options;

  constructor() {
    this.options = {};
  }

  async get(urn, data) {
    await this.setDefaultOptions("GET", undefined);
    if (!!data) urn += "?" + this.objectToQueryString(data);
    return this.send(urn);
  }

  async send(urn) {
    const uri = `${BASE_URL}/${urn}`;
    return fetch(uri, this.options)
      .then((res) => {
        if (res.ok) return res;
        return this.handleError(res);
      })
      .then((res) => {
        if (res.status === 204) return res.ok;

        if (res.url.includes("file")) {
          return res.arrayBuffer().then((blob) => {
            return new Blob([blob], {
              type: "application/octet-stream",
            });
          });
        } else {
          return res.json();
        }
      });
  }

  async setDefaultOptions(method, data) {
    this.options.method = method;
    this.options.body = JSON.stringify(data);
    this.options.headers = DEFAULT_HEADERS;
    this.options.headers["Content-Type"] = "multipart/form-data; boundary=<calculated when request is sent>";
  }

  handleError(res) {
    switch (res.status) {
      default:
        return res.json().then((err) => {
          return Promise.reject(err instanceof Object ? err.errors : err);
        });
    }
  }

  objectToQueryString(obj) {
    return Object.keys(obj)
      .map((key) => key + "=" + obj[key])
      .join("&");
    }
  }

export default new Request();
