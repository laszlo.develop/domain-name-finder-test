import React, { useEffect, useMemo, useState } from "react";

import Request from "./services/request";

export const useDomainSearch = () => {
  const [mainDomains, setMainDomains] = useState([]);
  const [domains, setDomains] = useState([]);
  const [selectedDomain, setSelectedDomain] = useState(null);
  const [selectedMainDomain, setSelectedMainDomain] = React.useState("");
  const [filteredDomains, setFilteredDomains] = useState([]);
  const [loading, setLoading] = useState(true);

  /** Fetch domains list from API and set it to the state */
  useEffect(() => {
    const fetchDomains = async () => {
      const domains = await Request.get("domains/search");
      setDomains(domains.domains);
      setFilteredDomains(domains.domains);
      setLoading(false);
    };
    fetchDomains();
  }, []);

  /** Set main domains list after domains is fetched */
  useEffect(() => {
    setMainDomains([
      ...new Set(domains.map((item) => item.domain.split(".").pop())),
    ]);
  }, [domains]);

  /** Filter domains list when main domain is selected */
  useEffect(() => {
    setFilteredDomains(
      domains.filter((i) => i.domain.includes(selectedMainDomain))
    );
  }, [selectedMainDomain, domains]);

  const handleChangeMainDomain = (event) => {
    setSelectedMainDomain(event.target.value);
  };

  /** Memoize domains list to avoid unnecessary re-renders */
  const domainsMemo = useMemo(() => filteredDomains, [filteredDomains]);

  return {
    domainsMemo,
    domains,
    loading,
    selectedDomain,
    setSelectedDomain,
    mainDomains,
    filteredDomains,
    selectedMainDomain,
    setSelectedMainDomain,
    handleChangeMainDomain,
  };
};
